#!/usr/bin/env python
#
# test_normal_usage.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import              glob
import itertools as it
import os.path   as op
import              shutil

import numpy   as np
import nibabel as nib

from fsl.data.image     import Image
from fsl.data.fixlabels import loadLabelFile
from fsl.utils.tempdir  import tempdir

from pyfix              import (io,
                                feature)
from pyfix.tests        import (main,
                                unzip,
                                compare_arrays,
                                compare_4D_data,
                                crop,
                                DATA_ARCHIVE,
                                FIXDIR_ARCHIVE,
                                TRAINDATA_ARCHIVE,
                                TRAINED_MODEL,
                                CLEANED_DATA_ARCHIVE,
                                CIFTI_FILE,
                                CLEANED_CIFTI_ARCHIVE)


def test_print_help():
    """Test the following commands:
      - fix
      - fix -h
    """
    main()
    main('-h')


def test_feature_extraction():
    """Test the following commands:
      - fix -f rest.ica
    """
    with tempdir():
        unzip(DATA_ARCHIVE)
        unzip(FIXDIR_ARCHIVE)

        main('-f rest.ica')

        got = io.read_features(op.join('rest.ica', 'fix', 'features.csv'))
        exp = io.read_features(op.join(            'fix', 'features.csv'))

        for col in feature.FEATURE_LABELS:
            gotcol = got[col]
            expcol = exp[col]
            if np.all(np.isclose(gotcol, expcol)):
                continue

            # fftfinerwrtnull generates a random null
            # distribution, so results are expected to
            # differ
            if col.startswith('fftfinerwrtnull'): err = 0.12
            else:                                 err = 0.01

            compare_arrays(gotcol, expcol, err)
            corr = gotcol.corr(expcol)
            assert corr > 0.99


def test_train():
    """Test the following commands:
      - fix -t pyfix_training    *.ica
      - fix -t pyfix_training -l *.ica
    """

    with tempdir():
        unzip(TRAINDATA_ARCHIVE)

        indirs = ' '.join(sorted(glob.glob('*.ica')))

        main(f'-t training1 {indirs}')
        assert op.exists('training1.pyfix_model')

        main(f'-t training2 -l {indirs}')
        assert op.exists('training2.pyfix_model')
        assert op.exists('training2_LOO_results')


def test_classify():
    """Test the following commands:
      - fix -c rest.ica training.pyfix_model <threshold>
      - fix -C training.pyfix_model *.ica
    """

    with tempdir():

        icadir = '1.ica'
        unzip(TRAINDATA_ARCHIVE)
        main(f'-c {icadir} {TRAINED_MODEL} 30')

        model     = op.splitext(op.basename(TRAINED_MODEL))[0]
        labelfile = io.generate_label_filename(icadir, TRAINED_MODEL, 30)

        assert op.exists(labelfile)
        loadLabelFile(labelfile)

        thresholds = [1, 2, 5, 10]
        icadirs    = list(glob.glob('?.ica'))

        main(f'-C {TRAINED_MODEL} '
             f'-t {",".join(map(str, thresholds))} '
             f'{" ".join(icadirs)}')

        for icadir, threshold in  it.product(icadirs, thresholds):
            labelfile = io.generate_label_filename(
                icadir, TRAINED_MODEL, threshold)
            assert op.exists(labelfile)


def test_cleanup():
    """Test the following commands:
      - fix -a labels.txt mel.ica
      - fix -a labels.txt mel.ica -m
      - fix -a labels.txt mel.ica -A
    """

    flag_variants = ['', '-m', '-A', '-m -h 10' , '-m -A']

    for flags in flag_variants:
        with tempdir():
            unzip(DATA_ARCHIVE)
            unzip(CLEANED_DATA_ARCHIVE)

            meldir    = io.InputDirectory.from_input_dir('rest.ica')
            labelfile = meldir.input_tree.get('hand_labels')

            main(f'-a {labelfile} {flags}')

            got        = meldir.input_tree.get('filtered_func_data_clean')
            gotvn      = meldir.input_tree.get('filtered_func_data_clean_vn')
            gotmot     = meldir.input_tree.get('motion_params_nifti')
            gotmotfilt = meldir.input_tree.get('motion_params_filtered')
            cleandir   = ('clean' + ' ' + flags).strip()
            cleandir   = cleandir.replace(' ', '_')
            exp        = op.join(cleandir, op.basename(got))
            expvn      = op.join(cleandir, op.basename(gotvn))
            expmot     = op.join(cleandir, op.basename(gotmot))
            expmotfilt = op.join(cleandir, op.basename(gotmotfilt))

            got   = Image(got).data
            exp   = Image(exp).data
            gotvn = Image(gotvn).data
            expvn = Image(expvn).data

            roiparams = np.loadtxt('fslroi_params.txt', dtype=int)
            got       = crop(got,   roiparams)
            gotvn     = crop(gotvn, roiparams)

            assert np.all(gotvn.shape == expvn.shape)
            compare_arrays(gotvn, expvn)
            compare_4D_data(got, exp)

            if op.exists(expmot):
                gotmot = Image(gotmot).data
                expmot = Image(expmot).data
                compare_4D_data(gotmot, expmot)
            if op.exists(expmotfilt):
                gotmotfilt = Image(gotmotfilt).data
                expmotfilt = Image(expmotfilt).data
                compare_4D_data(gotmotfilt, expmotfilt)


def test_cleanup_cifti():
    """Test cleaning commands on a dataset containing CIfTI time series. """

    flag_variants = ['-h -1', '-h 10', '-m -h -1', '-m -h 10']

    for flags in flag_variants:
        with tempdir():
            unzip(DATA_ARCHIVE)
            unzip(CLEANED_CIFTI_ARCHIVE)
            shutil.copy(CIFTI_FILE, 'rest.ica')

            meldir    = io.InputDirectory.from_input_dir('rest.ica')
            labelfile = meldir.input_tree.get('hand_labels')

            main(f'-a {labelfile} {flags}')

            cleandir   = ('clean' + ' ' + flags).strip()
            cleandir   = cleandir.replace(' ', '_')
            got        = meldir.input_tree.get('cifti_clean')
            exp        = op.join(cleandir, op.basename(got))

            got   = nib.load(got).get_fdata()
            exp   = nib.load(exp).get_fdata()

            assert np.all(got.shape == exp.shape)
            compare_arrays( got, exp)
            compare_4D_data(got, exp)


def test_full_run():
    """Test the following commands:
      - fix rest.ica training.pyfix_model <threshold>
    """
    with tempdir():
        unzip(DATA_ARCHIVE)
        unzip(FIXDIR_ARCHIVE, 'rest.ica')
        main(f'rest.ica {TRAINED_MODEL} 20')

        meldir = io.InputDirectory.from_input_dir('rest.ica')
        got    = meldir.input_tree.get('filtered_func_data_clean')

        assert op.exists(got)

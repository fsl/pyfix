#!/usr/bin/env python

if __name__ == '__main__':
    import sys
    from pyfix import fix
    sys.exit(fix.main())
